from django.db import models

# Create your models here.

class Tarefas(models.Model):
    TarefaId = models.BigAutoField(primary_key=True)
    TarefaTitulo = models.CharField(max_length=100)
    TarefaDescricao = models.CharField(max_length=500)
    #TarefaAtivo = models.
    TarefaData_Alteracao = models.DateField()
    TarefaData_Cadastro = models.DateField()

class Contatos(models.Model):
    ContatoId = models.BigAutoField(primary_key=True)
    ContatoNome = models.CharField(max_length=100)
    ContatoEmail = models.CharField(max_length=100)
    ContatoData_Alteracao =  models.DateField()
    ContatoData_Cadastro =  models.DateField()
