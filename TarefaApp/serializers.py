from rest_framework import serializers
from TarefaApp.models import Tarefas, Contatos

class TarefasSerializer(serializers.ModelSerializer):
    class Meta:
        model=Tarefas
        fields=(
            'TarefaId', 
            'TarefaTitulo',
            'TarefaDescricao',
            'TarefaData_Alteracao',
            'TarefaData_Cadastro',
        )

class ContatosSerializer(serializers.ModelSerializer):
    class Meta:
        model=Contatos
        fields=(
            'ContatoId',
            'ContatoNome',
            'ContatoEmail',
            'ContatoData_Alteracao',
            'ContatoData_Cadastro',
        )