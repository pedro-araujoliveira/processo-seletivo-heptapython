from django.conf.urls import url
from TarefaApp import views


urlpatterns=[
    url(r'^tarefa$', views.TarefaApi),
    url(r'^tarefa/([0-9]+)$', views.TarefaApi),

    url(r'^contato$', views.ContatoApi),
    url(r'^contato/([0-9]+)$', views.ContatoApi)
]
    
