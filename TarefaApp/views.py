from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from TarefaApp.models import Tarefas, Contatos
from TarefaApp.serializers import TarefaAppSerializer, ContatoSerializer

from django.core.files.storage import default_storage

# Create your views here.

@csrf_exempt
def TarefaApi(request,id=0):
    if request.method=='GET':
        tarefas = Tarefas.objects.all()
        tarefas_serializer=TarefaAppSerializer(tarefas,many=True)
        return JsonResponse(tarefas_serializer.data,safe=False)
    elif request.method=='POST':
        tarefas_data=JSONParser().parse(request)
        tarefas_serializer=TarefaAppSerializer(data=tarefas_data)
        if tarefas_serializer.is_valid():
            tarefas_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add",safe=False)
    elif request.method=='PUT':
        tarefas_data=JSONParser().parse(request)
        tarefas=Tarefas.objects.get(TarefaId=tarefas_data['TarefaId'])
        tarefas_serializer=TarefaAppSerializer(tarefas,data=tarefas_data)
        if tarefas_serializer.is_valid():
            tarefas_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update")
    elif request.method=='DELETE':
        tarefas=Tarefas.objects.get(TarefaId=id)
        tarefas.delete()
        return JsonResponse("Deleted Successfully",safe=False)

@csrf_exempt
def ContatoApi(request,id=0):
    if request.method=='GET':
        contatos = contatos.objects.all()
        contatos_serializer=ContatoSerializer(contatos,many=True)
        return JsonResponse(contatos_serializer.data,safe=False)
    elif request.method=='POST':
        contato_data=JSONParser().parse(request)
        contatos_serializer=ContatoSerializer(contato, data=contato_data)
        if contatos_serializer.is_valid():
            contatos_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add",safe=False)
    elif request.method=='PUT':
        contato_data=JSONParser().parse(request)
        contato=Contatos.objects.get(ContatoId=contato_data['ContatoId'])
        contatos_serializer=ContatoSerializer(contato, data=contato_data)
        if contatos_serializer.is_valid():
            contatos_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update")
    elif request.method=='DELETE':
        contatos=Contatos.objects.get(EmployeeId=id)
        contato.delete()
        return JsonResponse("Deleted Successfully",safe=False)

@csrf_exempt
def SaveFile(request):
    file=request.FILES['file']
    file_name=default_storage.save(file.name,file)
    return JsonResponse(file_name,safe=False)