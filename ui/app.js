const routes=[
    {path:'/home',component:home},
    {path:'/contato',component:contato},
    {path:'/tarefa',component:tarefa}
]

const router=new VueRouter({
    routes
})

const app = new Vue({
    router
}).$mount('#app')