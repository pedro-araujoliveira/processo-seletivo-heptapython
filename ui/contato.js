const contato={template:`
<div>

<button type="button"
class="btn btn-primary m-2 fload-end"
data-bs-toggle="modal"
data-bs-target="#exampleModal"
@click="addClick()">
 Add Contato
</button>

<table class="table table-striped">
<thead>
    <tr>
        <th>
            Contato Id
        </th>
        <th>
            Contato Nome
        </th>
        <th>
            Contato Email
        </th>
        <th>
            Tarefa
        </th>
        <th>
            Data de Cadastro
        </th>
        <th>
            Data de Alteracao
        </th>
        <th>
            Options
        </th>
    </tr>
</thead>
<tbody>
    <tr v-for="emp in contatos">
        <td>{{emp.ContatoId}}</td>
        <td>{{emp.ContatoNome}}</td>
        <td>{{emp.ContatoEmail}}</td>
        <td>{{emp.Tarefa}}</td>
        <td>{{emp.DataDeCadastro}}</td>
        <td>{{emp.DataDeAlteracao}}</td>
        <td>
            <button type="button"
            class="btn btn-light mr-1"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
            @click="editClick(emp)">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>
            </button>
            <button type="button" @click="deleteClick(emp.ContatoId)"
            class="btn btn-light mr-1">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

        </td>
    </tr>
</tbody>
</thead>
</table>

<div class="modal fade" id="exampleModal" tabindex="-1"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered">
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{modalTitle}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"
        aria-label="Close"></button>
    </div>

    <div class="modal-body">
    <div class="d-flex flex-row bd-highlight mb-3">
        <div class="p-2 w-50 bd-highlight">
            <div class="input-group mb-3">
                <span class="input-group-text">Nome</span>
                <input type="text" class="form-control" v-model="ContatoNome">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Email</span>
                <input type="text" class="form-control" v-model="ContatoEmail">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Tarefa</span>
                <select class="form-select" v-model="Tarefa">
                    <option v-for="dep in tarefas">
                    {{dep.TarefaTitulo}}
                    </option>
                </select>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Data de Cadastro</span>
                <input type="date" class="form-control" v-model="DataDeCadastro">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Data de Alteracao</span>
                <input type="date" class="form-control" v-model="DataDeAlteracao">
            </div>

        </div>
        
    </div>
        <button type="button" @click="createClick()"
        v-if="ContatoId==0" class="btn btn-primary">
        Create
        </button>
        <button type="button" @click="updateClick()"
        v-if="ContatoId!=0" class="btn btn-primary">
        Update
        </button>

    </div>

</div>
</div>
</div>


</div>


`,

data(){
    return{
        tarefas:[],
        contatos:[],
        modalTitle:"",
        ContatoId:0,
        ContatoNome:"",
        ContatoEmail:"",
        Tarefa:"",
        DataDeCadastro:"",
        DataDeAlteracao:""
    }
},
methods:{
    refreshData(){
        axios.get(variables.API_URL+"contato")
        .then((response)=>{
            this.contatos=response.data;
        });

        axios.get(variables.API_URL+"tarefas")
        .then((response)=>{
            this.tarefas=response.data;
        });
    },
    addClick(){
        this.modalTitle="Add Contato";
        this.ContatoId=0;
        this.ContatoNome="";
        this.ContatoEmail="";
        this.Tarefa="";
        this.DataDeCadastro="";
        this.DataDeAlteracao=""
    },
    editClick(emp){
        this.modalTitle="Edit Employee";
        this.ContatoId=emp.ContatoId;
        this.ContatoNome=emp.ContatoNome;
        this.ContatoEmail=emp.ContatoEmail;
        this.Tarefa=emp.Tarefa;
        this.DataDeCadastro=emp.DataDeCadastro;
        this.DataDeAlteracao=emp.DataDeAlteracao
    },
    createClick(){
        axios.post(variables.API_URL+"contato",{
            ContatoNome:this.ContatoNome,
            ContatoEmail:this.ContatoEmail,
            Tarefa:this.Tarefa,
            DataDeCadastro:this.DataDeCadastro,
            DataDeAlteracao:this.DataDeAlteracao
        })
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        });
    },
    updateClick(){
        axios.put(variables.API_URL+"contato",{
            ContatoNome:this.ContatoNome,
            ContatoEmail:this.ContatoEmail,
            Tarefa:this.Tarefa,
            DataDeCadastro:this.DataDeCadastro,
            DataDeAlteracao:this.DataDeAlteracao
        })
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        });
    },
    deleteClick(id){
        if(!confirm("Confirma?")){
            return;
        }
        axios.delete(variables.API_URL+"contato/"+id)
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        });

    },
    

},
mounted:function(){
    this.refreshData();
}

}